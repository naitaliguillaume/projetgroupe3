#include "affichagebalise.h"
#include "lectureExtractionFichier.h"
#include "ui_affichagebalise.h"
#include <QProcess>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>
#include <QTime>
#include <QSettings>
#include <QDateTime>
#include <QLocale>
#include "capteurmer.h"
#include "lectureExtractionFichier.h"
#include <QDir>
#include <QThread>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QDebug>
#include <QFont>
#include "prevision.h"





affichageBalise::affichageBalise(QString h,QString p,QString un,QString v,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::affichageBalise)
{

    ui->setupUi(this);
    this->unite=un;
    this->ville=v;
    this->pays=p;
    this->heure=h;

    timer = new QTimer();
    timer->setInterval(1000); //1000ms = 1 sec
    timer->start();

    //Gestion de l'affichage de l'heure:
    affHeure();

    connect(timer, SIGNAL(timeout()), this, SLOT(affHeure()));
    /*###########################################################*/


    // Relevé des données de la balise située en Mer toutes les 5 mins:
    timer_2 = new QTimer(this);
    recupererFichier();
    connect(timer_2, SIGNAL(timeout()), this, SLOT(recupererFichier()));
    timer_2->start(300000);// 300000ms -> 5 min


    QThread::sleep(1);
    paramDefaut();

}



affichageBalise::~affichageBalise()
{
    delete ui;
    delete timer;
    delete timer_2;
    qDebug() << "Destruction de affichageBalise";
}


//*************************************Gère la fermeture de la fenêtre affichagebalise************************************************//
void affichageBalise::closeEvent(QCloseEvent *event)
{

    QMessageBox msgBox;
    msgBox.setText(tr("Quitter?"));
    msgBox.setInformativeText(tr("Etes vous vous sûr(e) de vouloir fermer?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Icon::Question);
    int reponse = msgBox.exec();
    switch(reponse)
    {
    case QMessageBox::Yes :
        event->accept();
        break;
    default:
        event->ignore();
        break;
    }

}

//*****************************************************************************************************************************************//



//******************Connexion par scp avec la Raspbery et récupération des données météo de la balise côté Linux/Windows*******************//
void affichageBalise::recupererFichier()
{
    // Verication du systeme d'exploitation
    QString os = QSysInfo::productType();
    QString Command;
    QString homePath=QDir::homePath();// Répertoire par défaut de la machine


    if (os == "windows")
    {
        //Commande Windows pour échanger avec la Raspbery pi
        QString cmd ="scp -i "+homePath+"/projetgroupe3/integration/id_rsa "+"-P 44010 pi@82.65.244.166:/home/pi/projet/result.txt "+homePath+"/projetgroupe3/integration/";
        QProcess::execute(cmd);
    }
    else
    {
        //Commande Linux pour échanger avec la Raspbery pi
        Command = "scp -P 44010 pi@82.65.244.166:/home/pi/projet/result.txt "+homePath+"/projetgroupe3/integration/";
        QProcess::execute(Command);
    }

    lectureExtractionFichier lect;

    QString initRes = homePath+"/projetgroupe3/integration/result.txt";// chemin d'emplacement du fichier txt des données de la balise en mer

    cap = lect.lireFichier(initRes.toStdString());// récupération des informations du capteur
    temp = QString::number(cap.GetTemperature(), 'f', 1);// récupération pression
    hum = QString::number(cap.GetHumidite());// récupération  humidité
    press = QString::number(cap.GetPression());// récupération Pression

    if(this->unite=="°C")
    {
        ui->lineEdit->setText(temp+" "+"°C");
    }
    else if(this->unite=="°F")
    {
        double tempe = temp.toDouble()*1.8+32;// conversion en Faranight
        ui->lineEdit->setText(QString::number(tempe, 'f', 1)+" "+"°F");
    }
    ui->lineEdit_3->setText(this->hum+" "+"%");
    qDebug() << "hum : " << hum;
    ui->lineEdit_2->setText(this->press+" "+"hPa");
    QString Command2 ="rm "+initRes; // suppression du fichier de résultat

    QProcess::execute(Command2);

}
//*****************************************************************************************************************************************//

void affichageBalise::paramDefaut()
{
    QThread::sleep(1);
    qDebug() << ville;

    qDebug() << "SSL ? " << QSslSocket::supportsSsl();
    //affichage
    QString units;
    if(this->unite=="°C")
    {
        units ="metric";
    }
    else if(this->unite=="°F")
    {
        units ="imperial";
    }
    QNetworkRequest request(QUrl("http://api.openweathermap.org/data/2.5/weather?q="
    +ville+","+pays+"&appid=b027394b430c4dddda14bb967c220660&units="+units));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


    QNetworkAccessManager nam;

    QNetworkReply *reply = nam.get(request);

    while(!reply->isFinished())
    {
    qApp->processEvents();
    }
    reply->deleteLater();

    QByteArray response_data = reply->readAll();

    qDebug() << "Size: " << response_data.size();

    //Conversion du ByteArryay en Json
    QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);

    QJsonObject jsonObject = jsonResponse.object();


    /**************** Coordonnee ville ************/

    QJsonValue jsonCoord = jsonObject["coord"];
    QJsonObject obj1 = jsonCoord.toObject();
    qDebug() << "Longitude: " << obj1["lon"].toDouble();
    lon = QString::number(obj1["lon"].toDouble());
    qDebug() << "Latitude: " << obj1["lat"].toDouble();
    lat = QString::number(obj1["lat"].toDouble());

    /**************** Temperature ville ************/

    QJsonValue jsonMain = jsonObject["main"];
    QJsonObject objtemp = jsonMain.toObject();
    qDebug() << "temperature : " << objtemp["temp"].toDouble();
    qDebug() << "T° min: " << objtemp["temp_min"].toDouble();
    qDebug() << "T° max: " << objtemp["temp_max"].toDouble();
    qDebug() << "Pression Atm: " << objtemp["pressure"].toDouble();

    /**************** ICONE ************/
    QJsonArray jsonIcon = jsonObject["weather"].toArray();
    QJsonValue jsonMain_3 = jsonIcon[0].toObject();
    qDebug() << "icon: " << jsonMain_3["icon"].toString();
    this->icon = jsonMain_3["icon"].toString();




    /**************** INFO VILLE ************/
    QJsonValue jsonSys = jsonObject["sys"];
    QJsonObject obj = jsonSys.toObject();
    this->pays = obj["country"].toString();
    qDebug() << "pays: " << obj["country"].toString();


    /****************AFFICHAGE DE LA INFO ************/

    qDebug() << " ville : " <<ville;
    QString temp1 = QString::number(objtemp["temp"].toDouble(), 'f', 1);
    QString humidite = QString::number(objtemp["humidity"].toDouble());

    ui->resultat->setText(tr("Code du pays: ") + this->pays + "\n\n" + tr("Ville : ") + ville+"\n\n"+tr("Température actuel : ") + temp1 +unite+"\n\n"
        +"Humidité : "+ humidite +"%");



    /*********************Json prévision à 5 jour *******************/

    qDebug()<<"********recuperationndes info méteo (openweather)***************************";

    QNetworkRequest request1(QUrl("https://api.openweathermap.org/data/2.5/onecall?lat="+lat+"&lon="+lon+"&exclude=hourly,minutely&appid=b027394b430c4dddda14bb967c220660&units="+units));

    request1.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


    QNetworkAccessManager nam1;

    QNetworkReply *reply1 = nam.get(request1);

    while(!reply1->isFinished())
    {
    qApp->processEvents();
    }
    reply1->deleteLater();

    QByteArray response_data1 = reply1->readAll();

    qDebug() << "Size: " << response_data.size();

    //Conversion du ByteArryay en Json
    QJsonDocument doc = QJsonDocument::fromJson(response_data1);
    QJsonObject rootObj = doc.object();
        QJsonArray ptsArray = rootObj.value("daily").toArray();

        qDebug() << "There are " << ptsArray.size() << "sets of points in the json file";
        double i=0;
        foreach(const QJsonValue & tempValue, ptsArray){

            QJsonObject tempObj = tempValue.toObject();
            QJsonValue tempValue_2 = tempObj["temp"];
            QJsonValue tempValuedt = tempObj["dt"];
            //qDebug() << QString::number(tempValuedt.toInt());
            QJsonObject tempValue_2_Obj = tempValue_2.toObject();
            min.append(tempValue_2_Obj["min"].toDouble());
            qDebug() << "min :" << min[i];
            max.append(tempValue_2_Obj["max"].toDouble());
            qDebug() << "max :" << max[i];
            day.append(tempValue_2_Obj["day"].toDouble());
            qDebug() << "day :" << max[i];
            dt.append(i);
            i++;
            if(i==6) break;
        }


    QString homePath=QDir::homePath();// Répertoire par défaut de la machine
    ui->icon->setPixmap(QPixmap(homePath+"/projetgroupe3/integration/icones/"+
                                    icon+"@2x.png"));
    //tracé de courbes avec qcustomplot library sur qt:
    ui->courbe->clearGraphs();
    ui->courbe->clearItems();
    ui->courbe->clearPlottables();
    QDate currentDate =  QDate::currentDate();
    qDebug() << "currentDate : " << currentDate;

    d.append(currentDate.toString("dd.MM.yyyy"));
    for(int j =1;j<6;j++)
    {
        d.append(currentDate.addDays(j).toString("dd.MM.yyyy"));
    }
    qDebug() << "currentDate : " << d[1];

    ui->courbe->legend->setVisible(true);
    ui->courbe->addGraph();
    ui->courbe->graph(0)->setPen(QPen(Qt::blue));

    ui->courbe->graph(0)->setName(QString(tr("Température min")));

    ui->courbe->graph(0)->addData(dt,min);
    ui->courbe->graph(0)->setScatterStyle(QCPScatterStyle::ssCross);

    ui->courbe->addGraph();
    ui->courbe->graph(1)->setPen(QPen(Qt::red));
    ui->courbe->graph(1)->addData(dt,max);

    ui->courbe->graph(1)->setName(QString(tr("Température max")));
    ui->courbe->graph(1)->setScatterStyle(QCPScatterStyle::ssCross);


    // ajout de légende sur les courbes:
    ui->courbe->xAxis->setLabel("Date");

    ui->courbe->yAxis->setLabel(tr("Température en ")+unite);

    ui->courbe->graph(1)->setScatterStyle(QCPScatterStyle::ssCross);

    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    textTicker->addTicks(dt, d);
    ui->courbe->xAxis->setTicker(textTicker);


    // plage d'affichage du graphique:
    ui->courbe->xAxis->setRange(0,6);
    ui->courbe->yAxis->setRange(-60, 122);
    ui->courbe->axisRect(0)->axis(QCPAxis::atLeft)->rescale();

    ui->titre->setText(tr("Prévision des températures de la ville sélectionnée sur 5 jours"));

    ui->courbe->setVisible(true);
    ui->courbe->replot();


}

//*****************************************************************************************************************************************//

void affichageBalise::affHeure()
{  
   if(this->heure=="12h")
   {
       this->format = "h:m:s ap";
   }
   else
   {
       this->format = "hh:mm:ss";
   }
   currentHeure =  QTime::currentTime();
   ui->lblHeure->setText(currentHeure.toString(this->format));
}
