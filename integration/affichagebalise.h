#ifndef AFFICHAGEBALISE_H
#define AFFICHAGEBALISE_H

#include "capteurmer.h"
#include "qcustomplot.h"
#include <QCloseEvent>
#include <QDialog>
#include <QString>
#include <QTime>

namespace Ui {
class affichageBalise;
}

class affichageBalise : public QDialog
{
    Q_OBJECT

public:
    //explicit affichageBalise(QWidget *parent = nullptr);
    explicit affichageBalise(QString, QString, QString, QString, QWidget *parent = nullptr);
    ~affichageBalise();
    void closeEvent(QCloseEvent *event);
    QString GetCnt()
    {
        return pays;
    }




private slots:
    void affHeure();
    void recupererFichier();
    void paramDefaut();


private:
    Ui::affichageBalise *ui;
    QTimer *timer;
    QTimer *timer_2;
    QTime currentHeure;
    QString format="hh:mm:ss";
    QString press;
    QString hum;
    QString temp;
    capteurMer cap;
    QString unite;
    QString ville;
    QString pays;
    QString heure;
    QString lon;
    QString lat;
    QString icon;
    QVector<double> day;
    QVector<double> min;
    QVector<double> max;
    QVector<QString> d;
    QVector<double> dt;


};

#endif // AFFICHAGEBALISE_H
