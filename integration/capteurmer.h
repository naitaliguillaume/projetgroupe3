#ifndef CAPTEURMER_H
#define CAPTEURMER_H


class capteurMer
{
public:
    capteurMer(double=0,double=0,double=0);
    virtual ~capteurMer();

    double GetTemperature()
    {
        return temperature;
    }

    double GetHumidite()
    {
        return humidite;
    }

    double GetPression()
    {
        return pression;
    }

    void SetTemperature(double val)
    {
        temperature = val;
    }

    void SetHumidite(double val)
    {
        humidite = val;
    }

    void SetPression(double val)
    {
        pression = val;
    }


private:
    double temperature;
    double humidite;
    double pression;
};

#endif // CAPTEURMER_H
