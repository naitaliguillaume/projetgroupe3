#include "choixlangue.h"
#include "ui_choixlangue.h"
#include <QCloseEvent>
#include <QDir>
#include <QProcess>
#include <QSettings>
#include <QString>
#include <QTextStream>
#include <qmessagebox.h>
#include <QDebug>

choixLangue::choixLangue(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::choixLangue)
{
    ui->setupUi(this);
    this->langue = ui->langue->currentText();
}

choixLangue::~choixLangue()
{
    delete ui;
}


void choixLangue::on_langue_currentTextChanged(const QString &arg1)
{
    langue = arg1;
}

//*****************************************************************************************************************************************//
//Gère le changement de police
void choixLangue::on_pushButton_clicked()
{
    QString police = ui->police->currentText();
    QString Path = QDir::homePath()+"/projetgroupe3/integration/mesStyles.css";

    QFile file(Path);
    //Lecture de mesStyles.css
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
             return;
    QTextStream in(&file);
    QList<QString> line;
    while (!in.atEnd()) {
             line << in.readLine();
         }
    qDebug() << line.at(1);
    line[1]="  font-family: "+police+";";
    qDebug() << line.at(1);
    file.close();

    QFile f2(Path);
    //Ecriture dans mesStyles.css d'une liste de caractères
    if (!f2.open(QIODevice::ReadWrite | QIODevice::Text))
             return;
    QTextStream out(&f2);
    for (int i=0;i<line.size();i++)
    {
       out << line[i] << endl;
    }

    f2.close();
    close();
}
