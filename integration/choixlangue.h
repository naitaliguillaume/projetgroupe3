#ifndef CHOIXLANGUE_H
#define CHOIXLANGUE_H

#include <QDialog>
#include <QString>

namespace Ui {
class choixLangue;
}

class choixLangue : public QDialog
{
    Q_OBJECT

public:
    explicit choixLangue(QWidget *parent = nullptr);
    ~choixLangue();
    QString GetLangue()
    {
        return langue;
    }

private slots:
    void on_langue_currentTextChanged(const QString &arg1);
    void on_pushButton_clicked();



private:
    Ui::choixLangue *ui;
    QString langue;
};

#endif // CHOIXLANGUE_H
