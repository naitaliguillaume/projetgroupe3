#include "lectureExtractionFichier.h"
#include "capteurmer.h"
#include <qdebug.h>

lectureExtractionFichier::lectureExtractionFichier()
{

}

lectureExtractionFichier::~lectureExtractionFichier()
{
    cout << "Destruction (lectureExtractionFichier)" << endl;
}

//**********************************************Lecture du fichier texte issu de le Raspberry pi*************************************************//

capteurMer lectureExtractionFichier::lireFichier(string fichier)
{


    ifstream file(fichier);
    if (!file)
    {
        cout << "Impossible d'ouvrir le fichier en lecture ";
    }
    else
    {
        double temp;
        double hum;
        double pres;
        char c;

        file >> temp;
        file >> c;
        file >> hum;
        file >> c;
        file >> pres;


        this->cap.SetTemperature(temp);
        this->cap.SetHumidite(hum);
        this->cap.SetPression(pres);

    }

    file.close();
    return cap;

}

//*****************************************************************************************************************************************//
