#ifndef LECTUREEXTRACTIONFICHIER_H
#define LECTUREEXTRACTIONFICHIER_H
#include <iostream>
#include <fstream>
#include "capteurmer.h"
using namespace std;

class lectureExtractionFichier
{
    public:
        lectureExtractionFichier();
        virtual ~lectureExtractionFichier();
        capteurMer lireFichier(string);


    private:
        capteurMer cap;

};

#endif // LECTUREEXTRACTIONFICHIER_H
