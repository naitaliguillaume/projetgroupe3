#include "mainwindow.h"
#include "choixlangue.h"
#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include <QDebug>
#include <QSettings>
#include <QDir>
#include <QProcess>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFile fileCSS(":/styles/mesStyles.css");
    bool openOK = fileCSS.open(QIODevice::ReadOnly);

    if (openOK)
    {
        QString infosCSS = fileCSS.readAll();
        a.setStyleSheet(infosCSS);
        fileCSS.close();
    }


    QTextCodec::setCodecForLocale(QTextCodec::codecForName("Utf8"));//format d'affichage clavier azerty
    QString lg; // récupération langue
    QString initPath = QDir::homePath()+"/projetgroupe3/integration/params.ini"; // chemin du fichier de paramétrage
    QSettings maConfig(initPath, QSettings::IniFormat);
    choixLangue lang;
    lang.exec(); // lancement de la fenêtre choix langue
    lg=lang.GetLangue();
    qDebug() << lg;

    //**********************************************Récupération des informations de la langue*************************************************//

    if(lg=="Français")
    {
        maConfig.setValue("Langue/Langue", "français");
    }
    else
    {
        maConfig.setValue("Langue/Langue", "anglais");
    }
    QTranslator translator;
    if(maConfig.value("Langue/Langue").toString()=="anglais")
    {
        QString initUS = QDir::homePath()+"/projetgroupe3/integration/partieEnMer_en_US.qm";
        translator.load(initUS);
        a.installTranslator(&translator);
    }
    else
    {
        QString initFR = QDir::homePath()+"/projetgroupe3/integration/partieEnMer_en_FR.qm";
        translator.load(initFR);
        a.installTranslator(&translator);
    }

    //*****************************************************************************************************************************************//


    MainWindow w;
    QDesktopWidget* m = QApplication::desktop();
    QRect desk_rect = m->screenGeometry(m->screenNumber(QCursor::pos()));
    int desk_x = desk_rect.width();
    int desk_y = desk_rect.height();
    int x = w.width();
    int y = w.height();
    w.move(desk_x / 2 - x / 2 + desk_rect.left(), desk_y / 2 - y / 2 + desk_rect.top());
    w.show();
    return a.exec();
}
