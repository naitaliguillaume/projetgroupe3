#include "affichagebalise.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QString>
#include <QDebug>
#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QSettings>
#include <QDir>
#include <QObject>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Gère l'action de fermeture de mainwindow.ui:
void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox;
    msgBox.setText(tr("Quitter?"));
    msgBox.setInformativeText(tr("Etes vous vous sûr(e) de vouloir fermer?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Icon::Question);
    int reponse = msgBox.exec();
    switch(reponse)
    {
        case QMessageBox::Yes :
            event->accept();
            break;
        default:
            event->ignore();
            break;
     }
}
//********************************************************************************************************//

QString MainWindow::GetUnite()
{
    return ui->Unite->currentText(); // Récupération de l'unité de température
}



void MainWindow::on_actionQuitter_triggered()
{
    close();
}


// Lancement de l'application Windows / Linux qui permet l'affichage
// des données d'une balise en mer et des prévisions météo :
void MainWindow::on_Affichage_clicked()
{    
    affichageBalise aff(ui->heure->currentText(),ui->pays->currentText(),GetUnite(),ui->ville->text());
    QString initPath = QDir::homePath()+"/projetgroupe3/integration/params.ini";
    QMessageBox m;
    int r;

    if(aff.GetCnt()=="")
    {
            m.setText(tr("Attention vous n'avez pas rentré correctementle la ville ou choisie une présente dans ce pays!!!!!!!"));
            m.setInformativeText(tr("Veuillez rentrer un nom de ville correct."));
            m.setStandardButtons(QMessageBox::Ok);
            m.setDefaultButton(QMessageBox::Cancel);
            m.setIcon(QMessageBox::Icon::Question);
            r = m.exec();
    }
    else
    {
        aff.exec();
    }


}

