#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "affichagebalise.h"
#include <QMainWindow>
#include <QMouseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void closeEvent(QCloseEvent *) override;
    QString GetUnite();
    void delay(int);


private slots:
    void on_Affichage_clicked();
    void on_actionQuitter_triggered();



private:
    Ui::MainWindow *ui;
    affichageBalise *aff;

};
#endif // MAINWINDOW_H
