<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">MainWindow</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Accueil</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <source>Affichage Balise météo en mer et météo de la ville choisie</source>
        <oldsource>Affichage Balise météo en mer</oldsource>
        <translation type="vanished">Affichage Balise météo en mer et météo de la ville choisie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1476"/>
        <source>Unité de la température :</source>
        <translation>Unité de la température :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="64"/>
        <source>°F</source>
        <translation>°F</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>Choix de la ville :</source>
        <translation>Choix de la ville :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1454"/>
        <source>Code pays :</source>
        <translation>Code pays :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="127"/>
        <source>24h</source>
        <translation>24h</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="37"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <source>12h</source>
        <translation>12h</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <source>Choix de l&apos;affichage de l&apos;heure :</source>
        <translation>Choix de l&apos;affichage de l&apos;heure :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="mainwindow.ui" line="576"/>
        <source>FR</source>
        <translation>FR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>AF</source>
        <translation>AF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="191"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="196"/>
        <source>ZA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>CN</source>
        <translation>CN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <source>AX</source>
        <translation>AX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <source>AL</source>
        <translation>AL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <source>DZ</source>
        <translation>DZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="221"/>
        <source>DE</source>
        <translation>DE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="226"/>
        <source>DD</source>
        <translation>DD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <source>AD</source>
        <translation>AD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <source>AO</source>
        <translation>AO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="241"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="246"/>
        <source>AQ</source>
        <translation>AQ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="251"/>
        <source>AG</source>
        <translation>AG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <source>AN</source>
        <translation>AN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="266"/>
        <source>AR</source>
        <translation>AR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="276"/>
        <source>AW</source>
        <translation>AW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="281"/>
        <source>AU</source>
        <translation>AU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>AT</source>
        <translation>AT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="291"/>
        <source>AZ</source>
        <translation>AZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="296"/>
        <source>BS</source>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <source>BH</source>
        <translation>BH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="306"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <source>BB</source>
        <translation>BB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="316"/>
        <source>BY</source>
        <translation>BY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>BE</source>
        <translation>BE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="326"/>
        <source>BZ</source>
        <translation>BZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="331"/>
        <source>BJ</source>
        <translation>BJ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <source>BM</source>
        <translation>BM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>BO</source>
        <translation>BO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="351"/>
        <source>BQ</source>
        <translation>BQ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="356"/>
        <source>BA</source>
        <translation>BA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="361"/>
        <source>BW</source>
        <translation>BW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>BV</source>
        <translation>BV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="371"/>
        <source>BR</source>
        <translation>BR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="376"/>
        <source>BN</source>
        <translation>BN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <source>BG</source>
        <translation>BG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="386"/>
        <source>BF</source>
        <translation>BF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="391"/>
        <source>BI</source>
        <translation>BI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <source>CV</source>
        <translation>CV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <source>KY</source>
        <translation>KY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="406"/>
        <source>KH</source>
        <translation>KH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="411"/>
        <source>CM</source>
        <translation>CM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="416"/>
        <source>CX</source>
        <translation>CX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <source>CY</source>
        <translation>CY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>CC</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="431"/>
        <source>CO</source>
        <translation>CO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="436"/>
        <source>KM</source>
        <translation>KM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="441"/>
        <source>CG</source>
        <translation>cg</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="446"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="451"/>
        <source>CK</source>
        <translation>CK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>KR</source>
        <translation>KR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <source>KP</source>
        <translation>KP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="466"/>
        <source>CR</source>
        <translation>CR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="471"/>
        <source>CI</source>
        <translation>CI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="476"/>
        <source>HR</source>
        <translation>HR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <source>CU</source>
        <translation>CU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="486"/>
        <source>CW</source>
        <translation>CW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <source>DK</source>
        <translation>DK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>DJ</source>
        <translation>DJ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="501"/>
        <source>DO</source>
        <translation>DO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="506"/>
        <source>DM</source>
        <translation>DM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="511"/>
        <source>EG</source>
        <translation>EG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="516"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="521"/>
        <source>AE</source>
        <translation>AE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <source>EC</source>
        <translation>EC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="531"/>
        <source>ER</source>
        <translation>ER</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="536"/>
        <source>ES</source>
        <translation>ES</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="541"/>
        <source>EE</source>
        <translation>EE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="546"/>
        <source>US</source>
        <translation>US</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="556"/>
        <source>FK</source>
        <translation>FK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="561"/>
        <source>FO</source>
        <translation>FO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="566"/>
        <source>FJ</source>
        <translation>FJ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <source>FI</source>
        <translation>FI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <source>GA</source>
        <translation>GA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <source>GM</source>
        <translation>GM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <source>GE</source>
        <translation>GE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="596"/>
        <source>GS</source>
        <translation>GS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="601"/>
        <source>GH</source>
        <translation>GH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="606"/>
        <source>GI</source>
        <translation>GI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="611"/>
        <source>GR</source>
        <translation>GR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="616"/>
        <source>GD</source>
        <translation>GD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="621"/>
        <source>GL</source>
        <translation>GL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="626"/>
        <source>GP</source>
        <translation>GP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="631"/>
        <source>GU</source>
        <translation>GU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="636"/>
        <source>GT</source>
        <translation>GT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="641"/>
        <source>GG</source>
        <translation>GG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="646"/>
        <source>GN</source>
        <translation>GN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="651"/>
        <source>GW</source>
        <translation>GW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="656"/>
        <source>GQ</source>
        <translation>GQ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <source>GY</source>
        <translation>GY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="666"/>
        <source>GF</source>
        <translation>GF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <source>HT</source>
        <translation>HT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="676"/>
        <source>HM</source>
        <translation>HM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <source>HN</source>
        <translation>HN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="686"/>
        <source>HU</source>
        <translation>HU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="691"/>
        <source>IM</source>
        <translation>IM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="696"/>
        <source>UM</source>
        <translation>UM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="701"/>
        <source>VG</source>
        <translation>VG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="706"/>
        <source>VI</source>
        <translation>VI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="711"/>
        <source>IN</source>
        <translation>IN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="716"/>
        <source>IO</source>
        <translation>IO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="721"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <source>IR</source>
        <translation>IR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="731"/>
        <source>IQ</source>
        <translation>IQ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="741"/>
        <source>IS</source>
        <translation>IS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="746"/>
        <source>IL</source>
        <translation>IL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="751"/>
        <source>IT</source>
        <translation>IT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="756"/>
        <source>JM</source>
        <translation>JM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="761"/>
        <source>JP</source>
        <translation>JP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="766"/>
        <source>JE</source>
        <translation>JE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="771"/>
        <source>JO</source>
        <translation>JO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="776"/>
        <source>KZ</source>
        <translation>KZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="781"/>
        <source>KE</source>
        <translation>KE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <source>KG</source>
        <translation>KG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="791"/>
        <source>KI</source>
        <translation>KI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="796"/>
        <source>KW</source>
        <translation>KW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <source>LA</source>
        <translation>LA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="806"/>
        <source>LS</source>
        <translation>LS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="811"/>
        <source>LV</source>
        <translation>LV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="816"/>
        <source>LB</source>
        <translation>LB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="821"/>
        <source>LR</source>
        <translation>LR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="826"/>
        <source>LY</source>
        <translation>LY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="831"/>
        <source>LI</source>
        <translation>LI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="836"/>
        <source>LT</source>
        <translation>LT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="841"/>
        <source>LU</source>
        <translation>LU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="846"/>
        <source>MO</source>
        <translation>MO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="851"/>
        <source>MK</source>
        <translation>MK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="856"/>
        <source>MG</source>
        <translation>MG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="861"/>
        <source>MY</source>
        <translation>MY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="866"/>
        <source>MW</source>
        <translation>MW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <source>MV</source>
        <translation>MV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="876"/>
        <source>ML</source>
        <translation>ML</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="881"/>
        <source>MT</source>
        <translation>MT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="886"/>
        <source>MP</source>
        <translation>MP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="891"/>
        <source>MA</source>
        <translation>MA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="896"/>
        <source>MH</source>
        <translation>MH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="901"/>
        <source>MQ</source>
        <translation>MQ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="906"/>
        <source>MU</source>
        <translation>MU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="911"/>
        <source>MR</source>
        <translation>MR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="916"/>
        <source>YT</source>
        <translation>YT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="921"/>
        <source>MX</source>
        <translation>MX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="926"/>
        <source>FM</source>
        <translation>FM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="931"/>
        <source>MD</source>
        <translation>MD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="936"/>
        <source>MC</source>
        <translation>MC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="941"/>
        <source>MN</source>
        <translation>MN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="946"/>
        <source>ME</source>
        <translation>ME</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="951"/>
        <source>MS</source>
        <translation>MS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="956"/>
        <source>MZ</source>
        <translation>MZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="961"/>
        <source>MM</source>
        <translation>MM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="966"/>
        <source>NA</source>
        <translation>NA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="971"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="976"/>
        <source>NP</source>
        <translation>NP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="981"/>
        <source>NI</source>
        <translation>NI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="986"/>
        <source>NE</source>
        <translation>NE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="991"/>
        <source>NG</source>
        <translation>NG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>NU</source>
        <translation>NU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1001"/>
        <source>NF</source>
        <translation>NF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1006"/>
        <source>NO</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1011"/>
        <source>NC</source>
        <translation>NC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1016"/>
        <source>NZ</source>
        <translation>NZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1021"/>
        <source>OM</source>
        <translation>OM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1026"/>
        <source>UG</source>
        <translation>UG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1031"/>
        <source>UZ</source>
        <translation>UZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1036"/>
        <source>PK</source>
        <translation>PK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1041"/>
        <source>PW</source>
        <translation>PW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1046"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1051"/>
        <source>PA</source>
        <translation>PA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1056"/>
        <source>PG</source>
        <translation>PG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1061"/>
        <source>PY</source>
        <translation>PY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1066"/>
        <source>NL</source>
        <translation>NL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1071"/>
        <source>XX</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1076"/>
        <source>ZZ</source>
        <translation>ZZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1081"/>
        <source>PE</source>
        <translation>PE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1086"/>
        <source>PH</source>
        <translation>PH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1091"/>
        <source>PN</source>
        <translation>PN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1096"/>
        <source>PL</source>
        <translation>PL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1101"/>
        <source>PF</source>
        <translation>PF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1106"/>
        <source>PR</source>
        <translation>PR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1111"/>
        <source>PT</source>
        <translation>PT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1116"/>
        <source>QA</source>
        <translation>QA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1121"/>
        <source>SY</source>
        <translation>SY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1126"/>
        <source>CF</source>
        <translation>CF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1131"/>
        <source>RE</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1136"/>
        <source>RO</source>
        <translation>RO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1141"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1146"/>
        <source>RU</source>
        <translation>RU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1151"/>
        <source>RW</source>
        <translation>RW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1156"/>
        <source>EH</source>
        <translation>EH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1161"/>
        <source>BL</source>
        <translation>BL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1166"/>
        <source>KN</source>
        <translation>KN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1171"/>
        <source>SM</source>
        <translation>SM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1176"/>
        <source>MF</source>
        <translation>MF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1181"/>
        <source>SX</source>
        <translation>SX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1186"/>
        <source>PM</source>
        <translation>PM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1191"/>
        <source>VA</source>
        <translation>VA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1196"/>
        <source>VC</source>
        <translation>VC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1201"/>
        <source>SH</source>
        <translation>SH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1206"/>
        <source>LC</source>
        <translation>LC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <source>SB</source>
        <translation>SB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1216"/>
        <source>WS</source>
        <translation>WS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1221"/>
        <source>AS</source>
        <translation>AS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1226"/>
        <source>ST</source>
        <translation>ST</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1231"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1236"/>
        <source>RS</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1241"/>
        <source>SC</source>
        <translation>SC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1246"/>
        <source>SL</source>
        <translation>SL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1251"/>
        <source>SG</source>
        <translation>SG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1256"/>
        <source>SK</source>
        <translation>SK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1261"/>
        <source>SI</source>
        <translation>SI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1266"/>
        <source>SO</source>
        <translation>SO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1271"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1276"/>
        <source>SS</source>
        <translation>SS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1281"/>
        <source>LK</source>
        <translation>LK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1286"/>
        <source>SE</source>
        <translation>SE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1291"/>
        <source>CH</source>
        <translation>CH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1296"/>
        <source>SR</source>
        <translation>SR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1301"/>
        <source>SJ</source>
        <translation>SJ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1306"/>
        <source>TJ</source>
        <translation>TJ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1311"/>
        <source>TW</source>
        <translation>TW</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1316"/>
        <source>TZ</source>
        <translation>TZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1321"/>
        <source>TD</source>
        <translation>TD</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1326"/>
        <source>CZ</source>
        <translation>CZ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>TF</source>
        <translation>TF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1336"/>
        <source>TH</source>
        <translation>TH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1341"/>
        <source>TL</source>
        <translation>TL</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1346"/>
        <source>TG</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1351"/>
        <source>TK</source>
        <translation>TK</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1356"/>
        <source>TO</source>
        <translation>TO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1361"/>
        <source>TT</source>
        <translation>TT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1366"/>
        <source>TN</source>
        <translation>TN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1371"/>
        <source>TM</source>
        <translation>TM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1376"/>
        <source>TC</source>
        <translation>TC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1381"/>
        <source>TR</source>
        <translation>TR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1386"/>
        <source>TV</source>
        <translation>TV</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1391"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1396"/>
        <source>UY</source>
        <translation>UY</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1401"/>
        <source>VU</source>
        <translation>VU</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1406"/>
        <source>VE</source>
        <translation>VE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1411"/>
        <source>VN</source>
        <translation>VN</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1416"/>
        <source>WF</source>
        <translation>WF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1421"/>
        <source>YE</source>
        <translation>YE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1426"/>
        <source>ZM</source>
        <translation>ZM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1431"/>
        <source>ZW</source>
        <translation>ZW</translation>
    </message>
    <message>
        <source>Choix de la langue :</source>
        <translation type="vanished">Choix de la langue :</translation>
    </message>
    <message>
        <source>Français</source>
        <translation type="vanished">Français</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1491"/>
        <source>Fichier</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1500"/>
        <source>Quitter</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="31"/>
        <source>Quitter?</source>
        <translation>Quitter?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="32"/>
        <source>Etes vous vous sûr(e) de vouloir fermer?</source>
        <translation>Etes vous vous sûr(e) de vouloir fermer?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="73"/>
        <source>Attention vous n&apos;avez pas rentré correctementle la ville ou choisie une présente dans ce pays!!!!!!!</source>
        <translation>Attention vous n&apos;avez pas rentré correctementle la ville ou choisie une présente dans ce pays!!!!!!!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="74"/>
        <source>Veuillez rentrer un nom de ville correct.</source>
        <translation>Veuillez rentrer un nom de ville correct.</translation>
    </message>
</context>
<context>
    <name>affichageBalise</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialog</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="14"/>
        <source>Affichage des données météo</source>
        <translation>Affichage des données météo</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="51"/>
        <source>Données de la balise en mer</source>
        <translation>Affichage des données météo</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="119"/>
        <source>Température :</source>
        <translation>Température :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="141"/>
        <source>Taux d&apos;humidité :</source>
        <translation>Taux d&apos;humidité :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="200"/>
        <source>Prévisions météo par ville</source>
        <translation>Prévisions météo par ville</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="281"/>
        <source>Graphique Prévision des températures de la ville sélectionnée sur 5 jours</source>
        <translation>Graphique Prévision des températures de la ville sélectionnée sur 5 jours</translation>
    </message>
    <message>
        <source>Choix de la ville :</source>
        <translation type="vanished">Choix de la ville :</translation>
    </message>
    <message>
        <source>Valider</source>
        <translation type="vanished">Valider</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="72"/>
        <source>Pression :</source>
        <translation>Pression :</translation>
    </message>
    <message>
        <source>Format de l&apos;heure :</source>
        <translation type="vanished">Format de l&apos;heure :</translation>
    </message>
    <message>
        <source>24h</source>
        <translation type="vanished">24h</translation>
    </message>
    <message>
        <source>12h</source>
        <translation type="vanished">12h</translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="80"/>
        <source>Quitter?</source>
        <translation>Quitter?</translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="81"/>
        <source>Etes vous vous sûr(e) de vouloir fermer?</source>
        <translation>Etes vous vous sûr(e) de vouloir fermer?</translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="233"/>
        <source>Code du pays: </source>
        <translation>Code du pays: </translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="233"/>
        <source>Ville : </source>
        <translation>Ville : </translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="233"/>
        <source>Température actuel : </source>
        <translation>Température actuel : </translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="308"/>
        <source>Température min</source>
        <translation>Température min</translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="317"/>
        <source>Température max</source>
        <translation>Température max</translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="324"/>
        <source>Température en </source>
        <translation>Température en </translation>
    </message>
    <message>
        <location filename="affichagebalise.cpp" line="338"/>
        <source>Prévision des températures de la ville sélectionnée sur 5 jours</source>
        <translation>Prévision des températures de la ville sélectionnée sur 5 jours</translation>
    </message>
</context>
<context>
    <name>choixLangue</name>
    <message>
        <location filename="choixlangue.ui" line="14"/>
        <source>Langue</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="35"/>
        <source>Français</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="40"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="63"/>
        <source>Choix de la langue / choice of language :</source>
        <translation>Choix de la langue / choice of language :</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="85"/>
        <source>Choix de la police / choice of policy :</source>
        <translation>Choix de la police / choice of policy :</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="102"/>
        <source>Times</source>
        <translation>Times</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="107"/>
        <source>Arial</source>
        <translation>Arial</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="112"/>
        <source>AnyStyle</source>
        <translation>AnyStyle</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="117"/>
        <source>SansSerif</source>
        <translation>SansSerif</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="122"/>
        <source>Helvetica</source>
        <translation>Helvetica</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="127"/>
        <source>Serif</source>
        <translation>Serif</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="132"/>
        <source>TypeWriter</source>
        <translation>TypeWriter</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="137"/>
        <source>Courier</source>
        <translation>Courier</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="142"/>
        <source>OldEnglish</source>
        <translation>OldEnglishOldEnglish</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="147"/>
        <source>Monospace</source>
        <translation>Monospace</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="152"/>
        <source>Fantasy</source>
        <translation>Fantasy</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="157"/>
        <source>Cursive</source>
        <translation>Cursive</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="162"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="choixlangue.ui" line="179"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
</TS>
