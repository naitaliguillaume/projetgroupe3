#ifndef PREVISION_H
#define PREVISION_H
#include <QVector>

class prevision
{
public:
    prevision(QVector<double>,QVector<double>,QVector<double>);
    virtual ~prevision();

    QVector<double> GetDay()
    {
        return day;
    }

    QVector<double> GetMax()
    {
        return max;
    }

    QVector<double> GetMin()
    {
        return min;
    }

    void SetDay(QVector<double> val)
    {
        day = val;
    }

    void SetMax(QVector<double> val)
    {
        max = val;
    }

    void SetMin(QVector<double> val)
    {
        min = val;
    }


private:
    QVector<double> day;
    QVector<double> max;
    QVector<double> min;
};

#endif // PREVISION_H
