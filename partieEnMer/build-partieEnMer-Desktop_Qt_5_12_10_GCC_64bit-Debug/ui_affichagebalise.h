/********************************************************************************
** Form generated from reading UI file 'affichagebalise.ui'
**
** Created by: Qt User Interface Compiler version 5.12.10
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AFFICHAGEBALISE_H
#define UI_AFFICHAGEBALISE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_affichageBalise
{
public:
    QLabel *label;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QLabel *lblHeure;
    QLabel *label_4;
    QComboBox *heure;

    void setupUi(QDialog *affichageBalise)
    {
        if (affichageBalise->objectName().isEmpty())
            affichageBalise->setObjectName(QString::fromUtf8("affichageBalise"));
        affichageBalise->resize(799, 324);
        label = new QLabel(affichageBalise);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 40, 112, 24));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setItalic(true);
        font.setUnderline(true);
        font.setWeight(75);
        label->setFont(font);
        lineEdit_2 = new QLineEdit(affichageBalise);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setEnabled(false);
        lineEdit_2->setGeometry(QRect(190, 150, 131, 31));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        lineEdit_2->setFont(font1);
        lineEdit_2->setStyleSheet(QString::fromUtf8("background-color: #3A86DB;\n"
"color: white"));
        lineEdit_3 = new QLineEdit(affichageBalise);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setEnabled(false);
        lineEdit_3->setGeometry(QRect(240, 240, 131, 31));
        lineEdit_3->setFont(font1);
        lineEdit_3->setStyleSheet(QString::fromUtf8("background-color: #3A86DB;\n"
"color: white"));
        label_2 = new QLabel(affichageBalise);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(40, 240, 138, 24));
        label_2->setFont(font);
        lineEdit = new QLineEdit(affichageBalise);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setEnabled(false);
        lineEdit->setGeometry(QRect(180, 40, 131, 31));
        lineEdit->setFont(font1);
        lineEdit->setStyleSheet(QString::fromUtf8("background-color: #3A86DB;\n"
"color: white"));
        label_3 = new QLabel(affichageBalise);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(70, 150, 78, 24));
        label_3->setFont(font);
        lblHeure = new QLabel(affichageBalise);
        lblHeure->setObjectName(QString::fromUtf8("lblHeure"));
        lblHeure->setGeometry(QRect(590, 60, 171, 31));
        QFont font2;
        font2.setPointSize(10);
        lblHeure->setFont(font2);
        label_4 = new QLabel(affichageBalise);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(550, 10, 171, 51));
        label_4->setFont(font);
        heure = new QComboBox(affichageBalise);
        heure->addItem(QString());
        heure->addItem(QString());
        heure->setObjectName(QString::fromUtf8("heure"));
        heure->setGeometry(QRect(730, 20, 61, 31));
        heure->setFont(font2);

        retranslateUi(affichageBalise);

        QMetaObject::connectSlotsByName(affichageBalise);
    } // setupUi

    void retranslateUi(QDialog *affichageBalise)
    {
        affichageBalise->setWindowTitle(QApplication::translate("affichageBalise", "Dialog", nullptr));
        label->setText(QApplication::translate("affichageBalise", "Temp\303\251rature :", nullptr));
        lineEdit_3->setText(QString());
        label_2->setText(QApplication::translate("affichageBalise", "Taux d'humidit\303\251 :", nullptr));
        label_3->setText(QApplication::translate("affichageBalise", "Pression :", nullptr));
        lblHeure->setText(QString());
        label_4->setText(QApplication::translate("affichageBalise", "Format de l'heure :", nullptr));
        heure->setItemText(0, QApplication::translate("affichageBalise", "24h", nullptr));
        heure->setItemText(1, QApplication::translate("affichageBalise", "12h", nullptr));

    } // retranslateUi

};

namespace Ui {
    class affichageBalise: public Ui_affichageBalise {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AFFICHAGEBALISE_H
