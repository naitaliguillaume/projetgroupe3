/********************************************************************************
** Form generated from reading UI file 'choixlangue.ui'
**
** Created by: Qt User Interface Compiler version 5.12.10
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOIXLANGUE_H
#define UI_CHOIXLANGUE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_choixLangue
{
public:
    QDialogButtonBox *buttonBox;
    QComboBox *langue;
    QLabel *label_2;

    void setupUi(QDialog *choixLangue)
    {
        if (choixLangue->objectName().isEmpty())
            choixLangue->setObjectName(QString::fromUtf8("choixLangue"));
        choixLangue->resize(400, 300);
        buttonBox = new QDialogButtonBox(choixLangue);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(40, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);
        langue = new QComboBox(choixLangue);
        langue->addItem(QString());
        langue->addItem(QString());
        langue->setObjectName(QString::fromUtf8("langue"));
        langue->setGeometry(QRect(80, 110, 191, 31));
        QFont font;
        font.setPointSize(10);
        langue->setFont(font);
        label_2 = new QLabel(choixLangue);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 50, 331, 51));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setUnderline(true);
        font1.setWeight(75);
        label_2->setFont(font1);

        retranslateUi(choixLangue);
        QObject::connect(buttonBox, SIGNAL(accepted()), choixLangue, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), choixLangue, SLOT(reject()));

        QMetaObject::connectSlotsByName(choixLangue);
    } // setupUi

    void retranslateUi(QDialog *choixLangue)
    {
        choixLangue->setWindowTitle(QApplication::translate("choixLangue", "Dialog", nullptr));
        langue->setItemText(0, QApplication::translate("choixLangue", "Fran\303\247ais", nullptr));
        langue->setItemText(1, QApplication::translate("choixLangue", "English", nullptr));

        label_2->setText(QApplication::translate("choixLangue", "Choix de la langue / choice of language :", nullptr));
    } // retranslateUi

};

namespace Ui {
    class choixLangue: public Ui_choixLangue {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOIXLANGUE_H
