#include "affichagebalise.h"
#include "lectureExtractionFichier.h"
#include "ui_affichagebalise.h"
#include <QProcess>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>
#include <QTime>
#include <QSettings>
#include "capteurmer.h"
#include "lectureExtractionFichier.h"

affichageBalise::affichageBalise(QString ln,QString un,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::affichageBalise)
{
    ui->setupUi(this);
    this->unite=un;
    QSettings maConfig("/home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/params.ini", QSettings::IniFormat);
    maConfig.setIniCodec("UTF-8");

    timer = new QTimer();
    timer->setInterval(1000); //1000ms = 1 sec
    timer->start();

    //Gestion de l'affichage de l'heure:
    affHeure();

    connect(timer, SIGNAL(timeout()), this, SLOT(affHeure()));
    /*###########################################################*/


    //Relevé des données de la balise située en Mer toutes les 10 min:
    timer_2 = new QTimer(this);
    recupererFichier();
    connect(timer_2, SIGNAL(timeout()), this, SLOT(recupererFichier()));
    timer_2->start(300000);


}

affichageBalise::~affichageBalise()
{
    delete ui;
    delete timer;
    delete timer_2;
    qDebug() << "Destruction de affichageBalise";
}


void affichageBalise::closeEvent(QCloseEvent *event)
{
    QSettings maConfig("/home/naitalig/LanguageC++/projet_final_groupe3/partieEnMer/partieEnMer/params.ini", QSettings::IniFormat);
    if(maConfig.value("Langue/Langue").toString()=="anglais")
    {
        QMessageBox msgBox;
        msgBox.setText("Close?");
        msgBox.setInformativeText("Are you sure you want to quit?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Icon::Question);
        int reponse = msgBox.exec();
        switch(reponse)
        {
            case QMessageBox::Yes :
                event->accept();
                break;
            default:
                event->ignore();
                break;
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Quitter?");
        msgBox.setInformativeText("Etes vous vous sûr(e) de vouloir fermer?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Icon::Question);
        int reponse = msgBox.exec();
        switch(reponse)
        {
            case QMessageBox::Yes :
                event->accept();
                break;
            default:
                event->ignore();
                break;
        }
    }

}


void affichageBalise::recupererFichier()
{
    QString Command;    //Contains the command to be executed
    QString Command2;
    Command = "scp -P 44010 pi@82.65.244.166:/home/pi/projet/result.txt /home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/";
    QProcess::execute(Command);

    lectureExtractionFichier lect;
    cap = lect.lireFichier("/home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/result.txt");
    temp = QString::number(cap.GetTemperature(), 'f', 1);
    hum = QString::number(cap.GetHumidite());
    press = QString::number(cap.GetPression());
    qDebug() << hum;

    if(this->unite=="°C")
    {
        ui->lineEdit->setText(temp+" "+"°C");
    }
    else if(this->unite=="°F")
    {
        double tempe = temp.toDouble()*1.8+32;
        ui->lineEdit->setText(QString::number(tempe, 'f', 1)+" "+"°F");
    }
    ui->lineEdit_3->setText(this->hum+" "+"%");
    ui->lineEdit_2->setText(this->press+" "+"hPa");
    Command2 = "rm /home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/result.txt";
    QProcess::execute(Command2);

}


void affichageBalise::affHeure()
{
    currentHeure =  QTime::currentTime();
    ui->lblHeure->setText(currentHeure.toString(this->format));
}



void affichageBalise::on_heure_currentTextChanged(const QString &arg1)
{
    if(arg1=="12h")
    {
        this->format = "h:m:s ap";
    }
    else
    {
        this->format = "hh:mm:ss";
    }
}
