#ifndef AFFICHAGEBALISE_H
#define AFFICHAGEBALISE_H

#include "capteurmer.h"

#include <QCloseEvent>
#include <QDialog>
#include <QString>
#include <QTime>

namespace Ui {
class affichageBalise;
}

class affichageBalise : public QDialog
{
    Q_OBJECT

public:
    //explicit affichageBalise(QWidget *parent = nullptr);
    explicit affichageBalise(QString, QString, QWidget *parent = nullptr);
    ~affichageBalise();
    void closeEvent(QCloseEvent *event);




private slots:
    void affHeure();
    void on_heure_currentTextChanged(const QString &arg1);
    void recupererFichier();



private:
    Ui::affichageBalise *ui;
    QTimer *timer;
    QTimer *timer_2;
    QTime currentHeure;
    QString format="hh:mm:ss";
    QString press;
    QString hum;
    QString temp;
    capteurMer cap;
    QString unite;
};

#endif // AFFICHAGEBALISE_H
