#include "choixlangue.h"
#include "ui_choixlangue.h"

#include <QProcess>

choixLangue::choixLangue(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::choixLangue)
{
    ui->setupUi(this);
    this->langue = ui->langue->currentText();
}

choixLangue::~choixLangue()
{
    delete ui;
}


void choixLangue::on_langue_currentTextChanged(const QString &arg1)
{
    langue = arg1;
}
