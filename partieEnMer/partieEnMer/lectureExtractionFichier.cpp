#include "lectureExtractionFichier.h"
#include "capteurmer.h"

#include <qdebug.h>

lectureExtractionFichier::lectureExtractionFichier()
{

}

lectureExtractionFichier::~lectureExtractionFichier()
{
    cout << "Destruction (lectureExtractionFichier)" << endl;
}

capteurMer lectureExtractionFichier::lireFichier(string fichier)
{


    ifstream file(fichier);
    if (!file)
    {
        cout << "Impossible d'ouvrir le fichier en lecture ";
    }
    else
    {
        double temp;
        double hum;
        double pres;
        char c;

        file >> temp;
        file >> c;
        file >> hum;
        file >> c;
        file >> pres;


        this->cap.SetTemperature(temp);
        this->cap.SetHumidite(hum);
        this->cap.SetPression(pres);

        //qDebug() << QString::number(temp) << " " << QString::number(hum) << " " << QString::number(pres) << "\n";
    }

    file.close();
    return cap;

}
