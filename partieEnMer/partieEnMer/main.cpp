#include "mainwindow.h"
#include "choixlangue.h"
#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include <QDebug>
#include <QSettings>
#include <QSysInfo>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("Utf8"));
    QString lg;
    QSettings maConfig("/home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/params.ini", QSettings::IniFormat);

    choixLangue lang;
    lang.exec();
    lg=lang.GetLangue();
    qDebug() << lg;

    if(lg=="Français")
    {
        maConfig.setValue("Langue/Langue", "français");
    }
    else
    {
        maConfig.setValue("Langue/Langue", "anglais");
    }



    QTranslator translator;
    if(maConfig.value("Langue/Langue").toString()=="anglais")
    {
        translator.load("/home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/partieEnMer_en_US.qm");
        a.installTranslator(&translator);
    }
    else
    {
        translator.load("/home/naitalig/LanguageC++/projetgroupe3/partieEnMer/partieEnMer/partieEnMer_fr_FR.qm");
        a.installTranslator(&translator);
    }
    QString str = QSysInfo::productType();
    qDebug() << str;
    MainWindow w;
    w.show();
    return a.exec();

}
