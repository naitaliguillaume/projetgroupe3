#include "affichagebalise.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QString>
#include <QDebug>
#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings maConfig("/home/naitalig/LanguageC++/projet_final_groupe3/partieEnMer/partieEnMer/params.ini", QSettings::IniFormat);
    if(maConfig.value("Langue/Langue").toString()=="anglais")
    {
        QMessageBox msgBox;
        msgBox.setText("Close?");
        msgBox.setInformativeText("Are you sure you want to quit?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Icon::Question);
        int reponse = msgBox.exec();
        switch(reponse)
        {
            case QMessageBox::Yes :
                event->accept();
                break;
            default:
                event->ignore();
                break;
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Quitter?");
        msgBox.setInformativeText("Etes vous vous sûr(e) de vouloir fermer?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Icon::Question);
        int reponse = msgBox.exec();
        switch(reponse)
        {
            case QMessageBox::Yes :
                event->accept();
                break;
            default:
                event->ignore();
                break;
        }
    }
}

QString MainWindow::GetUnite()
{
    return ui->Unite->currentText();
}

void MainWindow::changeLang()
{

}


void MainWindow::on_actionQuitter_triggered()
{
    close();
}



void MainWindow::on_Affichage_clicked()
{
    if (aff == nullptr)
    {
        aff = new affichageBalise(lang,GetUnite(),this);
    }
    aff->exec();
    delete aff  ;
    aff = nullptr;
}
