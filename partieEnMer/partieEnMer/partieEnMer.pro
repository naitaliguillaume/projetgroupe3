QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    affichagebalise.cpp \
    capteurmer.cpp \
    choixlangue.cpp \
    lectureExtractionFichier.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    affichagebalise.h \
    capteurmer.h \
    choixlangue.h \
    lectureExtractionFichier.h \
    mainwindow.h

FORMS += \
    affichagebalise.ui \
    choixlangue.ui \
    mainwindow.ui

TRANSLATIONS = partieEnMer_fr_FR.ts partieEnMer_en_US.ts

CODECFORSRC     = UTF-8

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    sshPassword
