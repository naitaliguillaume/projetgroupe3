<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>Affichage Balise météo en mer</source>
        <translation>Sea weather beacon display</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Unité de la température :</source>
        <translation>Temperature unit :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="80"/>
        <source>°F</source>
        <translation>°F</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <source>Choix de la langue :</source>
        <translation>Choice of language :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="122"/>
        <source>Français</source>
        <translation>Fench</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="127"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <source>Fichier</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="152"/>
        <source>Quitter</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>affichageBalise</name>
    <message>
        <location filename="affichagebalise.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="35"/>
        <source>Température :</source>
        <translation>Temperature :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="110"/>
        <source>Taux d&apos;humidité :</source>
        <translation>Humidity level :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="157"/>
        <source>Pression :</source>
        <translation>pressure :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="197"/>
        <source>Format de l&apos;heure :</source>
        <translation>Time format :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="216"/>
        <source>24h</source>
        <translation>24h</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="221"/>
        <source>12h</source>
        <translation>12h</translation>
    </message>
</context>
</TS>
