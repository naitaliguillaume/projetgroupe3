<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>Affichage Balise météo en mer</source>
        <translation>Affichage Balise météo en mer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Unité de la température :</source>
        <translation>Unité de la température :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="80"/>
        <source>°F</source>
        <translation>°F</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <source>Choix de la langue :</source>
        <translation>Choix de la langue :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="122"/>
        <source>Français</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="127"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <source>Fichier</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="152"/>
        <source>Quitter</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>affichageBalise</name>
    <message>
        <location filename="affichagebalise.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="35"/>
        <source>Température :</source>
        <translation>Température :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="110"/>
        <source>Taux d&apos;humidité :</source>
        <translation>Taux d&apos;humidité :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="157"/>
        <source>Pression :</source>
        <translation>Pression :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="197"/>
        <source>Format de l&apos;heure :</source>
        <translation>Format de l&apos;heure :</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="216"/>
        <source>24h</source>
        <translation>24h</translation>
    </message>
    <message>
        <location filename="affichagebalise.ui" line="221"/>
        <source>12h</source>
        <translation>12h</translation>
    </message>
</context>
</TS>
